# Felix HTTP demos
## ApacheCon 2014 NA

This project contains several demonstrations of the functionality provided by
Felix HTTP bundle(s). Use the `run.bndrun` file in the *application* project to
run the demonstration.

NOTE: in order to get the SPDY demo correctly running, you need to add the
correct npn library on the bootclasspath, see README.md in the `npn/` directory
for more information!

