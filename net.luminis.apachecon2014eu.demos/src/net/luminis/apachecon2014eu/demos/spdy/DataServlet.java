package net.luminis.apachecon2014eu.demos.spdy;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

public class DataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private final Bundle m_bundle = FrameworkUtil.getBundle(getClass());

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pathInfo = getNormalizedPath(request);

		if ("true".equals(request.getHeader("x-spdy-push"))) {
			System.out.printf("[%TT] Requested through SPDY push: %s.%n", new Date(), pathInfo);
		} else {
			System.out.printf("[%TT] Requested through plain request: %s.%n", new Date(), pathInfo);
		}
		
		URL res = m_bundle.getResource(pathInfo);
		if (res != null) {
			response.setContentType(getContentType(pathInfo));

			streamResource(res, response);
		} else {
			response.sendError(HttpServletResponse.SC_NOT_FOUND, "No such resource!");
		}
	}

	private String getNormalizedPath(HttpServletRequest request) {
		String pathInfo = request.getPathInfo();
		if (pathInfo == null || "".equals(pathInfo) || "/".equals(pathInfo)) {
			pathInfo = "/spdy/index.html";
		}
		String servletPath = request.getServletPath();
		if (!pathInfo.startsWith(servletPath)) {
			pathInfo = servletPath.concat(pathInfo);
		}
		return pathInfo;
	}

	private String getContentType(String pathInfo) {
		if (pathInfo.endsWith(".html") || pathInfo.endsWith(".htm")) {
			return "text/html";
		} else if (pathInfo.endsWith(".png")) {
			return "image/png";
		} else if (pathInfo.endsWith(".css")) {
			return "text/css";
		} else if (pathInfo.endsWith(".js")) {
			return "application/javascript";
		}
		return "text/plain";
	}

	private void streamResource(URL resource, HttpServletResponse response) throws ServletException, IOException {
		try (InputStream is = resource.openStream(); OutputStream os = response.getOutputStream()) {
			byte[] buffer = new byte[65535];
			int len;
			while ((len = is.read(buffer)) != -1) {
				os.write(buffer, 0, len);
			}
		}
	}
}
