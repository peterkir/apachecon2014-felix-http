package net.luminis.apachecon2014eu.demos.ws;

import static net.luminis.apachecon2014eu.demos.ws.Constants.*;

import java.io.IOException;

import org.eclipse.jetty.websocket.WebSocket;

public class WSClient implements WebSocket.OnTextMessage {
	private final String m_id;
	private final WSRegistry m_registry;

	private Connection m_connection;
	private String m_nickName;

	public WSClient(WSRegistry registry) {
		m_registry = registry;
		m_id = String.format("user%d", m_registry.getClientCount() + 1);
	}

	public void disconnect() {
		m_connection.close();
	}

	public void onClose(int closeCode, String message) {
		if (m_registry.remove(this)) {
			System.out.printf("WebSocket closed with code (%d).%n", closeCode);
		}
	}

	public void onMessage(String data) {
		if (data == null || "".equals(data.trim())) {
			return;
		}

		String[] commands = data.split(",");
		for (int i = 0; i < commands.length;) {
			int cmd = Integer.parseInt(commands[i++]);
			switch (cmd) {
			case COMMAND_COLOR:
				i++; // Skip color
				break;
			case COMMAND_MESSAGE:
				i++; // Skip msg
				break;
			case COMMAND_MOUSEDOWN:
				i++; // Skip state
				break;
			case COMMAND_POSITION:
				i += 2; // Skip X,Y
				break;
			case COMMAND_SETNICKNAME:
				m_nickName = commands[i++];
				break;
			}
		}

		m_registry.broadcastCommand(this, data);
	}

	public String getId() {
		return m_id;
	}

	public String getNickName() {
		return m_nickName;
	}

	public int getLevel() {
		return 0; // XXX
	}

	public void onOpen(Connection connection) {
		m_connection = connection;
		if (m_registry.add(this)) {
			System.out.printf("WebSocket opened for protocol: %s...%n", connection.getProtocol());
		}
	}

	public void sendMessage(String data) throws IOException {
		m_connection.sendMessage(data);
	}
}