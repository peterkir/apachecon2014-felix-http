package net.luminis.apachecon2014eu.demos.ws;

public interface Constants
{
    int USER_LIST = 0;
    int USER_ID = 1;
    int USER_CONNECTED = 2;
    int USER_DISCONNECTED = 3;
    int COMMAND = 4;
    
    int COMMAND_SETNICKNAME = 0;
    int COMMAND_POSITION = 1;
    int COMMAND_MOUSEDOWN = 2;
    int COMMAND_MESSAGE = 3;
    int COMMAND_COLOR = 4;
}
