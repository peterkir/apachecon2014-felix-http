/* master control */
var EVENT_SLIDE_CHANGED = "esc", EVENT_FRAG_SHOWN = "efs", EVENT_FRAG_HIDDEN = "efh", EVENT_OVERVIEW_SHOWN = "eos", EVENT_OVERVIEW_HIDDEN = "eoh", EVENT_CLIENT_COUNT = "ecc";
var wsConn;

notifyServer = function(name, data) {
	if (!wsConn || wsConn.readyState != 1 /*WebSocket.OPEN*/) {
		return;
	}
	if (data) {
	    wsConn.send(name + "," + data);
	} else {
	    wsConn.send(name);
	}
}

// Tell reveal to notify our listeners for several changes...
Reveal.addEventListener("slidechanged", function(e) { notifyServer(EVENT_SLIDE_CHANGED, Reveal.getIndices().h + "," + Reveal.getIndices().v + "," + (Reveal.getIndices().f || 0)); });
Reveal.addEventListener("fragmentshown", function(e) { notifyServer(EVENT_FRAG_SHOWN, e.fragment.id); });
Reveal.addEventListener("fragmenthidden", function(e) { notifyServer(EVENT_FRAG_HIDDEN, e.fragment.id); });
Reveal.addEventListener("overviewshown", function(e) { notifyServer(EVENT_OVERVIEW_SHOWN); });
Reveal.addEventListener("overviewhidden", function(e) { notifyServer(EVENT_OVERVIEW_HIDDEN); });
// Open the connection to our server...
Reveal.addEventListener("ready", function(event) {
	var wsURL = (window.location.protocol == 'https:' ? 'wss://' : 'ws://') + window.location.host + window.location.pathname;
	wsURL = wsURL.replace(/\/?(index.html)?$/, '') + "/slidemgr";
    wsConn = new WebSocket(wsURL, [ "slides" ]);

    wsConn.onmessage = function(event) {
    	var msg = 'SSID: Felix, Pwd: felixdemo; go to: http://10.61.0.161:8080';
		var data = event.data.split(',');
		var name = data[0];
		switch (name) {
			case EVENT_CLIENT_COUNT:
				document.getElementById("client-count").innerHTML = data[1] > 0 ? msg + "; active viewers: " + data[1] : msg;
				break;
		}
    }
} );

function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}