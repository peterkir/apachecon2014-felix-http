# npn-boot JARs

This directory contain various releases of the npn-boot JAR needed for SPDY to
work correctly.

A caveat of the current npn-boot implementation is that it is *very* tightly 
coupled to a JDK/JRE version. See the table later on for hints on what version
you need to deploy.

## Usage

Add the npn-boot JAR to the boot classpath of the JVM by specifying

    -runvm: -Xbootclasspath/p:../etc/npn-boot/npn-boot-<VERSION>.jar

in your bnd file. Additionally, you need to specify that the 
"org.eclipse.jetty.npn" package should be present on the boot-delegation path:

    -runproperties: org.osgi.framework.bootdelegation="org.eclipse.jetty.npn"

Lastly, you need to add the "org.amdatu.http.npn-spi" bundle to your list of 
run bundles. This is a fragment that exposes the "org.eclipse.jetty.npn" 
package from the system bundle.

# NPN vs. OpenJDK versions

Below is a table found on the NPN wiki page (see [1]):

| NPN version     | (Open)JDK version
| 1.0.0.v20120402 | 1.7.0 - 1.7.0u2 - 1.7.0u3
| 1.1.0.v20120525 | 1.7.0u4 - 1.7.0u5
| 1.1.1.v20121030 | 1.7.0u6 - 1.7.0u7
| 1.1.3.v20130313 | 1.7.0u9 - 1.7.0u10 - 1.7.0u11
| 1.1.4.v20130313 | 1.7.0u13
| 1.1.5.v20130313 | 1.7.0u15 - 1.7.0u17 - 1.7.0u21 - 1.7.0u25
| 1.1.6.v20130911 | 1.7.0u40 - 1.7.0u45 - 1.7.0u51
| 1.1.7-SNAPSHOT  | ?


# References

1. http://www.eclipse.org/jetty/documentation/current/npn-chapter.html#npn-versions
